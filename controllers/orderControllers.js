const Order   = require("../models/Order");
const Product = require("../models/Product");
const User    = require("../models/User");
const auth    = require("../auth");

module.exports.addOrder = (req,res)=>{
	//console.log(req.body);

	let newOrder = new Order({
		totalAmount: req.body.totalAmount,
		quantity   : req.body.quantity,
		products   : [{productId: req.body.productId, quantity: req.body.quantity, price : req.body.price}],
		userId 	   : req.body.userId,


	})

	Product.findById(req.body.productId)
		   .then(foundProduct => { 
		   	console.log(foundProduct);
		   	if(isAdmin = false){
				return res.send({message: "Admins can't place order"})
			}
			newOrder.save()
				    .then(result => res.send({message: "Order Successful"}))
				    .catch(error => res.send(error))
		})
}
