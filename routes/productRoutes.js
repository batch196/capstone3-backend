

const express = require("express");
const router = express.Router();

const productControllers = require("../controllers/productControllers");

const auth = require("../auth");

const { verify,verifyAdmin } = auth;



//add new products
router.post("/addProducts",verify,verifyAdmin,productControllers.addProduct);

// //get all active products
router.get('/activeProducts',productControllers.getActiveProducts);

//get single product
router.get('/getSingleProduct/:productId',productControllers.getSingleProduct);

//update product by id
router.put("/updateProduct/:productId",verify,verifyAdmin,productControllers.updateProduct);

//archive product
router.put("/archiveProduct/:productId",verify,verifyAdmin,productControllers.archiveProduct);

//activate product
router.put("/activateProduct/:productId",verify,verifyAdmin,productControllers.activateProduct);

//get all products
router.get("/getAllProducts",verify,verifyAdmin,productControllers.getAllProducts);

//delete One product
router.delete("/deleteProduct/:productId",verify,verifyAdmin,productControllers.deleteProduct);




module.exports = router;
